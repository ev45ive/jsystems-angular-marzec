import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClockComponent } from './components/clock/clock.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { LoginComponent } from './containers/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CensorDirective } from './validators/censor.directive';
import { RouterModule } from '@angular/router';
import { FormMessagesComponent } from './components/form-messages/form-messages.component';

@NgModule({
  declarations: [
    ClockComponent,
    NavBarComponent,
    LoginComponent,
    CensorDirective,
    FormMessagesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  exports: [
    // ClockComponent,
    NavBarComponent,
    LoginComponent,
    FormsModule,
    ReactiveFormsModule,
    CensorDirective,
    FormMessagesComponent
  ]
})
export class SharedModule { }
