import { Directive, ElementRef, Input, Optional, Self, SimpleChanges } from '@angular/core';
import { AbstractControl, NgModel, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
  selector: '[appCensor]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: CensorDirective,
      multi: true
    },
  ]
})
export class CensorDirective implements Validator {

  @Input('appCensor') badword = 'batman'

  constructor(
    private elem: ElementRef<HTMLInputElement>,
    // @Optional() @Self() private model: NgModel
  ) {
    console.log('hello from appCensor', elem.nativeElement);
    // console.log(model);

    this.elem.nativeElement.style.color = 'red'
  }

  validate(control: AbstractControl): ValidationErrors | null {
    if (control.value.null) return null;

    if (typeof control.value !== 'string') {
      throw new Error('appCensor only works with strings')
    }

    return control.value.includes(this.badword) ? {
      // required:true
      // error: { message : ' .... ' }
      'censor': this.badword
    } : null
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.

  }

}
