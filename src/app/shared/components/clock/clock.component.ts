import { ChangeDetectionStrategy, ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { Observable, timer } from 'rxjs';

@Component({
  selector: 'app-clock',
  template: `
    <span>      {{time}}    </span>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClockComponent implements OnInit {

  time = ''

  constructor(
    private zone: NgZone,
    private cdr: ChangeDetectorRef) {
    // this.cdr.detach()

    this.updateTime()
  }

  private updateTime() {
    this.time = (new Date()).toLocaleTimeString();
  }

  ngOnInit(): void {
    // this.cdr.detectChanges()

    this.zone.runOutsideAngular(()=>{

      setInterval(() => {
        this.updateTime()
        this.cdr.detectChanges()
      }, 1000)
    })

  }

}
