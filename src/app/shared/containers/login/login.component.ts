import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {

  user = {
    username: '',
    email: ''
  }

  constructor() { }

  ngOnInit(): void {
  }

  alert() {
    // NG0100: ExpressionChangedAfterItHasBeenCheckedError: 
    // Expression has changed after it was checked.
    this.user.username = 'changed';
    console.log('detect changes');
  }

}
