import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlaylistsContainer } from './containers/playlists/playlists.container';
import { PlaylistsComponent } from './playlists.component';

const routes: Routes = [
  {
    path: '', component: PlaylistsComponent,
    children: [
      {
        path: '', component: PlaylistsContainer
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule { }
