import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsComponent } from './playlists.component';
import { PlaylistsContainer } from './containers/playlists/playlists.container';
import { PlaylistsListComponent } from './components/playlists-list/playlists-list.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistEditorComponent } from './components/playlist-editor/playlist-editor.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    PlaylistsComponent,
    PlaylistsContainer,
    PlaylistsListComponent,
    PlaylistDetailsComponent,
    PlaylistEditorComponent
  ],
  imports: [
    CommonModule,
    PlaylistsRoutingModule,
    SharedModule
  ],
  exports: [
    // PlaylistsContainer
  ]
})
export class PlaylistsModule { }
