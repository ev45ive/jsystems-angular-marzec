import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, tap, filter, mergeAll, mergeMap, switchMap, catchError, EMPTY, share } from 'rxjs';
import { Album } from 'src/app/core/model/search';
import { MusicAPIService } from 'src/app/core/services/music-api/music-api.service';

@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.container.html',
  styleUrls: ['./album-search.container.scss']
})
export class AlbumSearchContainer implements OnInit {

  message = ''

  queryChanges = this.route.queryParamMap.pipe(
    map(params => params.get('q')),
    tap(() => this.message = ''),
    filter((q): q is string => q !== null))

  resultsChanges = this.queryChanges.pipe(
    switchMap(q => this.fetchResults(q)),
    share()
  )

  constructor(
    private service: MusicAPIService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void { }

  fetchResults(q: string) {
    return this.service.fetchSearchResults(q).pipe(
      catchError(error => {
        this.message = error.message; return EMPTY
      })
    )
  }

  search(query: string | null) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { q: query },
    })
  }
}
