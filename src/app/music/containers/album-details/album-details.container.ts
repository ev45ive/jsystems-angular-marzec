import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { map, share, switchMap } from 'rxjs';
import { MusicAPIService } from 'src/app/core/services/music-api/music-api.service';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.container.html',
  styleUrls: ['./album-details.container.scss']
})
export class AlbumDetailsContainer implements OnInit {

  album_id = this.route.queryParamMap.pipe(
    map(params => params.get('id'))
  )

  albumChange = this.album_id.pipe(
    switchMap(id => {
      if (!id) throw 'No Id';
      return this.service.fetchAlbumById(id)
    }),
    share()
  )

  constructor(
    private service: MusicAPIService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {

  }

}
